use bevy::{
    input::mouse::{MouseMotion, MouseScrollUnit, MouseWheel},
    prelude::*,
};

use crate::CelestialBody;

#[derive(Default)]
pub struct CameraPlugin(pub CameraTuning);

impl Plugin for CameraPlugin {
    fn build(&self, app: &mut App) {
        app.insert_resource(self.0).add_systems(Update, move_camera);
    }
}

pub fn move_camera(
    time: Res<Time>,
    tuning: Res<CameraTuning>,
    mouse: Res<ButtonInput<MouseButton>>,
    mut scroll: EventReader<MouseWheel>,
    mut motion: EventReader<MouseMotion>,
    mut camera: Query<(&mut Transform, &Parent), With<Camera3d>>,
    bodies: Query<&CelestialBody>,
) {
    let (mut camera, tracking) = camera.single_mut();
    let body = bodies.get(**tracking).unwrap();

    let mut zoom = 0.0;
    for ev in scroll.read() {
        zoom += match ev.unit {
            MouseScrollUnit::Line => 10.0 * ev.y,
            MouseScrollUnit::Pixel => ev.y,
        };
    }
    let mut rotation = Quat::IDENTITY;
    if mouse.pressed(MouseButton::Right) {
        for ev in motion.read() {
            rotation *= Quat::from_axis_angle(*camera.up(), -ev.delta.x * tuning.rotation_speed);
            rotation *= Quat::from_axis_angle(*camera.right(), -ev.delta.y * tuning.rotation_speed);
        }
    }

    zoom = tuning.scale_zoom(
        zoom,
        camera.translation.distance(Vec3::ZERO) - body.radius * tuning.min_zoom_mult,
    );
    let movement = camera.forward() * zoom * time.delta_seconds();

    camera.translation = (camera.translation + movement)
        .clamp_length(body.radius * tuning.min_zoom_mult, tuning.max_zoom);
    camera.rotate_around(Vec3::ZERO, rotation);
}

#[derive(Clone, Copy, Resource)]
pub struct CameraTuning {
    pub rotation_speed: f32,
    pub zoom_base: f32,
    pub zoom_power: f32,
    pub zoom_multiplier: f32,
    pub min_zoom_mult: f32, // A multiplier of the targeted body radius
    pub max_zoom: f32,
}

impl Default for CameraTuning {
    fn default() -> Self {
        Self {
            rotation_speed: 1.0 / 250.0,
            zoom_base: 100.0,
            zoom_power: 1.1,
            zoom_multiplier: 0.5,
            min_zoom_mult: 3.0,
            // max_zoom: 50000.0,
            max_zoom: 100_000_000.0,
        }
    }
}

impl CameraTuning {
    fn scale_zoom(&self, scroll: f32, dist: f32) -> f32 {
        scroll * self.zoom_multiplier * (dist.powf(self.zoom_power) + self.zoom_base)
    }
}
