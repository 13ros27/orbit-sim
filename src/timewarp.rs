use bevy::prelude::*;
use std::ops::Deref;

const TIMEWARPS: [f32; 15] = [
    0.0,
    1.0,
    2.0,
    3.0,
    4.0,
    5.0,
    10.0,
    25.0,
    50.0,
    100.0,
    1000.0,
    10_000.0,
    100_000.0,
    1000_000.0,
    10_000_000.0,
];

pub struct TimewarpPlugin;

impl Plugin for TimewarpPlugin {
    fn build(&self, app: &mut App) {
        app.insert_resource(Timewarp(1))
            .add_systems(Startup, setup_timewarp_ui)
            .add_systems(Update, change_timewarp);
    }
}

fn setup_timewarp_ui(mut commands: Commands) {
    commands.spawn((
        TextBundle::from_section(
            "1x",
            TextStyle {
                font_size: 30.0,
                ..default()
            },
        ),
        TimewarpText,
    ));
}

fn change_timewarp(
    input: Res<ButtonInput<KeyCode>>,
    mut timewarp: ResMut<Timewarp>,
    mut timewarp_ui: Query<&mut Text, With<TimewarpText>>,
) {
    let change =
        input.just_pressed(KeyCode::Period) as isize - input.just_pressed(KeyCode::Comma) as isize;

    if change != 0
        && (change != -1 || timewarp.0 != 0)
        && (change != 1 || timewarp.0 != TIMEWARPS.len() - 1)
    {
        timewarp.0 = (timewarp.0 as isize + change) as usize;
        let mut text = timewarp_ui.single_mut();
        *text = Text::from_section(
            format!("{}x", **timewarp as usize),
            TextStyle {
                font_size: 30.0,
                ..default()
            },
        );
    }
}

#[derive(Resource)]
pub struct Timewarp(usize);

impl Deref for Timewarp {
    type Target = f32;
    fn deref(&self) -> &f32 {
        &TIMEWARPS[self.0]
    }
}

pub fn not_paused(timewarp: Res<Timewarp>) -> bool {
    timewarp.0 != 0
}

#[derive(Component)]
struct TimewarpText;
