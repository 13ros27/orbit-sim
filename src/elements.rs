use bevy::prelude::*;
use std::f32::consts::TAU;

#[derive(Debug)]
pub struct Cartesian {
    pos: Vec3, // Position vector (km)
    vel: Vec3, // Velocity vector (km/s)
}

impl Cartesian {
    pub fn new(pos: Vec3, vel: Vec3) -> Self {
        Self { pos, vel }
    }

    // See: https://space.stackexchange.com/questions/1904/how-to-programmatically-calculate-orbital-elements-using-position-velocity-vecto/1919#1919
    pub fn as_keplerian(&self, mu: f32) -> Keplerian {
        // Compute the specific angular momentum, h
        let h = self.pos.cross(self.vel);
        // Compute the node vector, n = [0 0 1].cross(h)
        let n = Vec3::new(-h.y, h.x, 0.0);
        // Compute the radius, r, and velocity, v
        let r = self.pos.length();
        let v = self.vel.length();
        // Compute the eccentricity vector, e, and the eccentricity, |e|
        let e = ((v * v - mu / r) * self.pos - self.vel * self.pos.dot(self.vel)) / mu;
        let ecc = e.length();
        // Compute the specific mechanical energy, E
        let energy = (v * v) / 2.0 - mu / r;
        // Compute the semi-major axis, a (NB: We may want to deal with e == 1 here)
        let sma = -mu / (2.0 * energy);
        // Compute the inclination, i, the right ascension of the ascending node, $\Omega$,
        //  the argument of periapse, $\omega$, and the true anomaly, v.
        let inc = (h.z / h.length()).acos();
        let raan = (n.x / n.length()).acos();
        let arg_peri = (n.dot(e) / (n.length() * ecc)).acos();
        let anom = (e.dot(self.pos) / (ecc * r)).acos();
        // Compute eccentric anomaly, $\tau$
        let tau = (2.0 * ((1.0 - ecc) / (1.0 + ecc)).sqrt() * (anom / 2.0).tan()).atan();
        // Compute the time of periapse passage, t
        let time = (mu / sma.powi(3)).sqrt() * (tau - ecc * tau.sin());
        Keplerian {
            sma,
            ecc,
            inc,
            arg_peri,
            raan,
            time,
        }
    }
}

#[derive(Clone, Copy, Debug)]
pub struct Keplerian {
    sma: f32,      // Semi-major axis (km)
    ecc: f32,      // Eccentricity (0-1)
    inc: f32,      // Inclination (radians, 0-pi)
    arg_peri: f32, // Argument of periapsis (radians, 0-tau)
    raan: f32,     // Right ascension of the ascending node (radians, 0-tau)
    time: f32,     // Time of periapse passage (seconds)
}

impl Keplerian {
    pub fn as_ellipse(&self) -> (Vec2, Vec3, Quat) {
        let half_size = Vec2::new(self.sma, self.sma * (1.0 - self.ecc * self.ecc).sqrt());
        let rotation = Quat::from_axis_angle(Vec3::Z, self.raan)
            * Quat::from_axis_angle(Vec3::X, self.inc)
            * Quat::from_axis_angle(Vec3::Z, self.arg_peri);
        let centre = rotation * -Vec3::X * self.sma * self.ecc;
        (half_size, centre, rotation)
    }

    pub fn elapse_time(&mut self, mu: f32, elapsed: f32) {
        self.time = (self.time + elapsed) % self.period(mu);
        // self.time = self.time + elapsed;
        // println!(
        //     "new time = {:?}s, period = {:?}",
        //     self.time,
        //     self.period(mu)
        // );
    }

    pub fn position(&self, mu: f32) -> Vec3 {
        // Compute the mean, eccentric and true anomaly
        let mean_anomaly = (mu / self.sma.powi(3)).sqrt() * self.time;
        let eccentric_anomaly = self.mean_to_eccentric_anomaly(mean_anomaly);
        let true_anomaly = 2.0
            * (((1.0 + self.ecc) / (1.0 - self.ecc)).sqrt() * (eccentric_anomaly / 2.0).tan())
                .atan();
        let p = self.sma * (1.0 - self.ecc * self.ecc);
        // Compute the radius, r
        let r = p / (1.0 + self.ecc * true_anomaly.cos());
        // Compute argument of latitude, $\omega + \nu$
        let arg_lat = self.arg_peri + true_anomaly;
        // Compute the position
        Vec3::new(
            self.raan.cos() * arg_lat.cos() - self.raan.sin() * arg_lat.sin() * self.inc.cos(),
            self.raan.sin() * arg_lat.cos() - self.raan.cos() * arg_lat.sin() * self.inc.cos(),
            self.inc.sin() * arg_lat.sin(),
        ) * r
    }

    fn period(&self, mu: f32) -> f32 {
        TAU * (self.sma.powi(3) / mu).sqrt()
    }

    // See: https://rdcu.be/dEH51
    fn mean_to_eccentric_anomaly(&self, mean_anomaly: f32) -> f32 {
        let mut eccentric = mean_anomaly;
        let mut last_eccentric = 0.0;

        // Loop until it converges (within f32 numerical precision, typically around 5 loops)
        while last_eccentric != eccentric && !eccentric.is_nan() {
            last_eccentric = eccentric;
            eccentric -= (eccentric - mean_anomaly - self.ecc * eccentric.sin())
                / (1.0 - self.ecc * eccentric.cos());
        }

        eccentric
    }
}
