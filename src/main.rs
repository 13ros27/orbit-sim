#![warn(clippy::all)]
use bevy::{prelude::*, transform::TransformSystem};

use camera::CameraPlugin;
use debug::uv_debug_texture;
use elements::{Cartesian, Keplerian};
use timewarp::{not_paused, Timewarp, TimewarpPlugin};

mod camera;
mod debug;
mod elements;
mod timewarp;

fn main() {
    App::new()
        .add_plugins((
            DefaultPlugins.set(WindowPlugin {
                primary_window: Some(Window {
                    title: "Orbit Sim".into(),
                    ..default()
                }),
                ..default()
            }),
            CameraPlugin::default(),
            TimewarpPlugin,
        ))
        .insert_resource(AmbientLight {
            color: Color::WHITE,
            brightness: 1000.0,
        })
        .add_systems(Startup, (setup, propagate_bodies).chain())
        .add_systems(Update, propagate_bodies.run_if(not_paused))
        .add_systems(
            PostUpdate,
            draw_orbits.after(TransformSystem::TransformPropagate),
        )
        .run();
}

fn draw_orbits(
    mut gizmos: Gizmos,
    spaceships: Query<(&Orbit, &Parent)>,
    bodies: Query<&GlobalTransform, With<CelestialBody>>,
) {
    for (orbit, parent) in &spaceships {
        let body = bodies.get(**parent).unwrap();
        let (half_size, centre, rotation) = orbit.info.as_ellipse();
        gizmos
            .ellipse(
                centre + body.translation(),
                rotation,
                half_size,
                orbit.colour,
            )
            .segments(100);
    }
}

fn propagate_bodies(
    time: Res<Time>,
    timewarp: Res<Timewarp>,
    mut spaceships: Query<(&mut Transform, &mut Orbit, &Parent)>,
    bodies: Query<&CelestialBody>,
) {
    for (mut transform, mut orbit, parent) in &mut spaceships {
        let body = bodies.get(**parent).unwrap();
        orbit
            .info
            .elapse_time(body.mu, time.delta_seconds() * **timewarp);
        *transform = transform.with_translation(orbit.info.position(body.mu));
    }
}

fn setup(
    mut commands: Commands,
    mut meshes: ResMut<Assets<Mesh>>,
    mut images: ResMut<Assets<Image>>,
    mut materials: ResMut<Assets<StandardMaterial>>,
) {
    let debug_material = materials.add(StandardMaterial {
        base_color_texture: Some(images.add(uv_debug_texture())),
        ..default()
    });

    // Kerbol
    let kerbol_mu = 1172332800.0;
    commands
        .spawn((
            PbrBundle {
                mesh: meshes.add(Sphere::new(261600.0)),
                material: debug_material.clone(),
                transform: Transform::from_xyz(0.0, 0.0, 0.0),
                ..default()
            },
            CelestialBody {
                radius: 261600.0,
                mu: kerbol_mu,
            },
        ))
        .with_children(|parent| {
            // Kerbin
            let kerbin_mu = 3531.6;
            parent
                .spawn((
                    PbrBundle {
                        mesh: meshes.add(Sphere::new(600.0)),
                        material: debug_material,
                        ..default()
                    },
                    CelestialBody {
                        radius: 600.0,
                        mu: kerbin_mu,
                    },
                    Orbit::new(
                        kerbol_mu,
                        Vec3::new(13599840.256, 0.0, 0.0),
                        Vec3::new(0.0, 0.0, 9.285),
                        Color::GREEN,
                    ),
                ))
                .with_children(|parent| {
                    // Spaceship
                    parent.spawn((
                        PbrBundle {
                            mesh: meshes.add(Sphere::new(25.0)),
                            material: materials.add(Color::WHITE),
                            ..default()
                        },
                        Spaceship,
                        Orbit::new(
                            kerbin_mu,
                            Vec3::new(680.0, 0.0, 0.0),
                            Vec3::new(0.0, 0.0, 2.279),
                            // Vec3::new(0.0, 0.0, 3.3),
                            Color::RED,
                        ),
                    ));
                    // Camera
                    parent.spawn(Camera3dBundle {
                        transform: Transform::from_xyz(0.0, 12500.0, 0.0)
                            .looking_at(Vec3::ZERO, Vec3::Y),
                        ..default()
                    });
                });
        });
}

#[derive(Component)]
struct CelestialBody {
    radius: f32, // Radius (km)
    mu: f32,     // Standard gravitational parameter, $\mu$ (km^3/s^2)
}

#[derive(Component)]
struct Spaceship;

#[derive(Component)]
struct Orbit {
    colour: Color,
    info: Keplerian,
}

impl Orbit {
    fn new(mu: f32, pos: Vec3, vel: Vec3, colour: Color) -> Self {
        Self {
            colour,
            info: Cartesian::new(pos, vel).as_keplerian(mu),
        }
    }
}
